#!/bin/bash

if [ -d "powerline/fonts" ]; then
	cd "powerline/fonts"
	git pull origin master
	cd ..
else
	git clone 'https://github.com/powerline/fonts'
fi

chmod a+x ./fonts/install.sh
./fonts/install.sh
