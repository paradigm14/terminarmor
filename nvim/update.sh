#!/bin/bash

nvim +PluginUpdate +qall
nvim +PluginClean +qall

echo "Nvim Plugins updated!"
