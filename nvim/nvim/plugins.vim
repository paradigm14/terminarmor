Plugin 'benekastah/neomake'
Plugin 'Shougo/deoplete.nvim'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'flazz/vim-colorschemes'
Plugin 'hdima/python-syntax'
Plugin 'airblade/vim-gitgutter'
Plugin 'kien/ctrlp.vim'
Plugin 'xolox/vim-misc'
Plugin 'ervandew/supertab'
Plugin 'tpope/vim-commentary'
Plugin 'easymotion/vim-easymotion'
Plugin 'tpope/vim-fugitive'
Plugin 'the31k/vim-colors-tayra'
Plugin 'kshenoy/vim-signature'
Plugin 'mileszs/ack.vim'
Plugin 'jez/vim-superman'
Plugin 'alvan/vim-closetag'
Plugin 'jiangmiao/auto-pairs'
Plugin 'terryma/vim-multiple-cursors'

" Dependencies
Plugin 'Shougo/vimproc.vim'

" Haskell deoplete
Plugin 'eagletmt/neco-ghc'
Plugin 'dag/vim2hs'
Plugin 'eagletmt/ghcmod-vim'

" Javascript
Plugin 'pangloss/vim-javascript'
Plugin 'mxw/vim-jsx'

" Purescript
Plugin 'raichoo/purescript-vim'
Plugin 'frigoeu/psc-ide-vim'

" Python deoplete
Plugin 'zchee/deoplete-jedi'
