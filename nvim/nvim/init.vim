" Inside this sources the plugins.vim that contains installed plugins
source $HOME/.config/nvim/vundleSetup.vim

" Customizations
set number
colorscheme tayra
syntax on
set nowrap

" Fix indentations and tabs
set tabstop=4
set shiftwidth=4

" Use better looking arrows for airline
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''

let g:airline#extensions#tabline#left_sep = ''
let g:airline#extensions#tabline#left_alt_sep = ''

" Airline theme
let g:airline_theme='distinguished'

" Enable the list of buffers
let g:airline#extensions#tabline#enabled = 1

" Enable Deoplete (autocompletion) by default
let g:deoplete#enable_at_startup = 1
let g:deoplete#auto_completion_start_length = 0.00

" Run Neomake every file write
autocmd! BufWritePost * Neomake

" Retains selection after indenting
vnoremap < <gv
vnoremap > >gv

" Use F8 for TagBar toggle
nmap <F8> :TagbarToggle<CR>

" Fix for SuperTab reverse cycle selction
let g:SuperTabDefaultCompletionType = "<c-n>"

" Case insensitivity and smartcase
set ignorecase
set smartcase

" Better regex search
nnoremap / /\v
nnoremap ? ?\v

" Search ahead
set incsearch

" Tab navigation like Firefox.
nnoremap <C-t> :tabnew<CR>
inoremap <C-t> <ESC>:tabnew<CR>

nnoremap <Up> :bprevious<CR>
nnoremap <Down> :bnext<CR>
nnoremap <Left> :tabprevious<CR>
nnoremap <Right> :tabnext<CR>

inoremap <Up> <nop>
inoremap <Down> <nop>
inoremap <Left> <nop>
inoremap <Right> <nop>

" Quick Navigation by percentage
nnoremap <A-1> 10%zz
nnoremap <A-2> 20%zz
nnoremap <A-3> 30%zz
nnoremap <A-4> 40%zz
nnoremap <A-5> 50%zz
nnoremap <A-6> 60%zz
nnoremap <A-7> 70%zz
nnoremap <A-8> 80%zz
nnoremap <A-9> 90%zz

" Use Ag instead of Ack if available
if executable('ag')
  let g:ackprg = 'ag --nogroup --nocolor --column'
endif

" Ignore files for ctrlp
set wildignore+=*.pyc,*.class

" Quick align
noremap <leader>= kJa<BACKSPACE><CR><ESC>l

" Auto close tags with filenames like *.xml, *.html, *.xhtml, ...
let g:closetag_filenames = "*.html,*.xhtml,*.phtml,*.jsp"

" Disable conceal for vim2hs
let g:haskell_conceal = 0

" Enabled detailed ghc deoplete
let g:necoghc_enable_detailed_browse = 1

" Use spaces instead of tabs!
set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab

" Auto check and lint for ghc-mod ((Haskell))
autocmd BufWritePost *.hs GhcModCheckAndLintAsync

" Bind CTRL+ \ to close Quickfix window
nnoremap <C-\> :ccl<CR>

" CSS Autocomplete
autocmd FileType css set omnifunc=csscomplete#CompleteCSS

" Always run machine specific config last
if filereadable(glob("$HOME/.config/nvim/local.vim")) 
    source $HOME/.config/nvim/local.vim
else
    execute "! touch $HOME/.config/nvim/local.vim"
endif
