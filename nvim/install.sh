#!/bin/bash

mkdir -p "$HOME/config"
CONFIG_DIR=$HOME/.config

# Directory of this script
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Install Vundle for plugin management
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

# Copy all vim scripts
echo "Copying $DIR/nvim to $CONFIG_DIR"
cp -rf "$DIR/nvim" "$CONFIG_DIR"

nvim +PluginInstall +qall
nvim +UpdateRemotePlugins +qall

echo "Nvim Plugin installed!"
