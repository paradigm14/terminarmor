## Initialize Variables
() {
  local LC_ALL="" LC_CTYPE="en_US.UTF-8"
  SEG_END=$'\ue0b0'
  SEG_END_2=$'\ue0b2'
  NUM_SEGMENT=0
  PROMPT_LENGTH=0

  # Colorschemes
  SEGMENT1_CLR=190
  SEGMENT1_TEXT=255
  SEGMENT2_CLR=235
  SEGMENT2_TEXT=235
  SEGMENT3_CLR=237
}

set_fg() {
	echo -n "%F{$1}"
}

end_fg() {
	echo -n "%f"
}

set_bg() {
	echo -n "%K{$1}"
}

end_bg() {
	echo -n "%k"
}

segment_end() {
  local bg fg
  fg=$1
  bg=$2

  echo -n "%K{$bg}%F{$fg}$SEG_END%k%f"

  # Update global variable
  NUM_SEGMENT=$((NUM_SEGMENT + 1))
}

segment_end_backward() {
  local bg fg
  fg=$1
  bg=$2

  echo -n "%K{$bg}%F{$fg}$SEG_END_2%k%f"
  # Update global variable
  NUM_SEGMENT=$((NUM_SEGMENT + 1))
}

## Segment Builder
build_segment() {
  local text fg bg
  text=$1
  fg=$2
  bg=$3

  set_fg $fg
  set_bg $bg

  echo -n "$text"

  set_fg
  end_bg
}

## Draws a line
build_line() {
  local length color
  length=$1
  color=$2

  set_bg $color
  echo -n ${(r:$(($length)):: :)}
  end_bg
}

get_length() {
  local prompt zero
  prompt=$1

  zero='%([BSUbfksu]|([FBK]|){*})'
  PROMPT_LENGTH=${#${(S%%)prompt//$~zero/}} 
}

get_env() {
	local current_env

	if [[ -n $VIRTUAL_ENV ]]; then
		current_env=$(basename $VIRTUAL_ENV)
		echo " $current_env "
	else
		echo " "
	fi
}

get_branch() {
	local branch
	branch=$(git_prompt_info)

	if [[ -n $branch ]]; then
		echo " $branch "
	else
		echo " "
	fi
}

## Main prompt
build_prompt() {

  # Hostname
  build_segment " %n@%M " $SEGMENT2_CLR $SEGMENT1_CLR
  segment_end $SEGMENT1_CLR $SEGMENT2_CLR

  # Current directory
  build_segment " %1d " $SEGMENT1_TEXT $SEGMENT2_CLR
  segment_end $SEGMENT2_CLR $SEGMENT3_CLR

  # Calculates needed space
  get_length "_%n@%M__%1d_$(get_branch)$(get_env)"
  OFFSET=2
  remaining_space=$((COLUMNS - PROMPT_LENGTH - NUM_SEGMENT - OFFSET))

  # Draws line
  build_line $remaining_space $SEGMENT3_CLR

  # Git branch
  segment_end_backward $SEGMENT2_CLR $SEGMENT3_CLR
  build_segment "$(get_env)" $SEGMENT1_TEXT $SEGMENT2_CLR

  # Python virtual environment
  segment_end_backward $SEGMENT1_CLR $SEGMENT2_CLR
  build_segment "$(get_branch)" $SEGMENT2_CLR $SEGMENT1_CLR

  # Shell prompt
  set_fg $SEGMENT1_CLR
  echo -n " + "
  end_fg
}

PROMPT='$(build_prompt)'
