() {
  local LC_ALL="" LC_CTYPE="en_US.UTF-8"

  if [[ $UID == 0 || $EUID == 0 ]]; then
	  LINE_COLOR=001
  else
	  LINE_COLOR=006
  fi

  LINE_COLOR_2=003
  LABEL_CLR=006
  SEP_COLOR=008
}

line_decor() {
	echo "%F{$LINE_COLOR}┌${(r:$((COLUMNS-1))::─:)}%f"
}

# Args: linecolor prompt_text
line_prompt() {
	line_color=$1
	prompt_text=$2
	echo "%F{$line_color}─────────%f $prompt_text"
}

# Args: color_code text
color() {
	color_code=$1
	text=$2
	echo "%F{$color_code}$text%f"
}

line_git() {

  local PL_BRANCH_CHAR
  () {
    local LC_ALL="" LC_CTYPE="en_US.UTF-8"
    PL_BRANCH_CHAR=$'\ue0a0'         # 
  }
  local ref dirty mode repo_path
  repo_path=$(git rev-parse --git-dir 2>/dev/null)

  if $(git rev-parse --is-inside-work-tree >/dev/null 2>&1); then
    dirty=$(parse_git_dirty)
    ref=$(git symbolic-ref HEAD 2> /dev/null) || ref="➦ $(git rev-parse --short HEAD 2> /dev/null)"

    if [[ -e "${repo_path}/BISECT_LOG" ]]; then
      mode=" <B>"
    elif [[ -e "${repo_path}/MERGE_HEAD" ]]; then
      mode=" >M<"
    elif [[ -e "${repo_path}/rebase" || -e "${repo_path}/rebase-apply" || -e "${repo_path}/rebase-merge" || -e "${repo_path}/../.dotest" ]]; then
      mode=" >R>"
    fi

    setopt promptsubst
    autoload -Uz vcs_info

    zstyle ':vcs_info:*' enable git
    zstyle ':vcs_info:*' get-revision true
    zstyle ':vcs_info:*' check-for-changes true
    zstyle ':vcs_info:*' stagedstr '*'
    zstyle ':vcs_info:*' unstagedstr '!'
    zstyle ':vcs_info:*' formats ' %u%c'
    zstyle ':vcs_info:*' actionformats ' %u%c'
    vcs_info

    if [[ -n $dirty ]]; then
      branch_clr=001
    else
      branch_clr=002
    fi

	branch_text="${ref/refs\/heads\//$PL_BRANCH_CHAR }${vcs_info_msg_0_%% }${mode}"
	text="$(color $LABEL_CLR git) $(color $SEP_COLOR :) $(color $branch_clr $branch_text)"
    line_prompt $LINE_COLOR_2 $text
  fi
}

line_dir() {
	current_dir=$(pwd)
	current_dir_clr=$(echo "$current_dir" | sed 's/\//%F{006}\/%f/g')
	text="$(color $LABEL_CLR dir) $(color $SEP_COLOR :) $current_dir_clr"
	line_prompt $LINE_COLOR_2 $text
}

line_host() {
	host_inf="$(color 015 %n)$(color 006 @)$(color 003 %M) [$(color 006 %T)]"
	text="$(color $LABEL_CLR sys) $(color $SEP_COLOR :) $host_inf"
	line_prompt $LINE_COLOR_2 $text
}

# Virtualenv: current working virtualenv
line_virtualenv() {
  local virtualenv_path="$VIRTUAL_ENV"
  if [[ -n $virtualenv_path && -n $VIRTUAL_ENV_DISABLE_PROMPT ]]; then
	env_text=`basename $virtualenv_path`
	text="$(color $LABEL_CLR env) $(color $SEP_COLOR :) $(color 007 $env_text)"
    line_prompt $LINE_COLOR_2 $text
  fi
}

line_ipaddr() {
	OUT_STR=""
	interfaces=$(ifconfig | sed 's/[ \t].*//;/^$/d')

	for interface in $(echo $interfaces); do
		if [[ -n $interface && $interface != "Iface" && $interface != "lo" ]]; then
			ip_addr=$(ifconfig "$interface" | grep 'inet addr:' | cut -d: -f2| cut -d' ' -f1)

			if [[ -n $ip_addr ]]; then
				text="$(color 003 $interface)$(color $SEP_COLOR :)$(color 006 $ip_addr)"
				OUT_STR="$OUT_STR$text "
			fi
		fi
	done

	if [[ -n $OUT_STR ]]; then
		line_prompt $LINE_COLOR_2 "$(color $LABEL_CLR net) $(color $SEP_COLOR :) $OUT_STR"
	fi
}

line_shell() {
	echo "%F{$LINE_COLOR}└──%f %F{$LINE_COLOR_2}|%f "
}

## Main prompt
build_prompt() {
  line_decor
  line_git
  line_virtualenv
  line_ipaddr
  line_dir
  line_host
  line_shell
}

PROMPT='%{%f%b%k%}$(build_prompt)'
