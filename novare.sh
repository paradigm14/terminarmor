FREE_SPACE=$(( (COLUMNS - 50)/2 ))
SPACES=${(r:$(($FREE_SPACE)):: :)}

BLUE='\033[0;34m'
ORNG='\033[0;33m'
NC='\033[0m'

if [[ $COLUMNS -gt 50 ]]; then
	echo "${BLUE}"
	echo $SPACES "${ORNG}                    \`.\`  ....                   "
	echo $SPACES "${ORNG}                 -. :/-  ://+${BLUE}  /+/:.             "
	echo $SPACES "${ORNG}                 /. :/-  ::/+${BLUE}  /ooooo/.          "
	echo $SPACES "  \`/osssssssso   ${ORNG}-\`${BLUE}            :osssssso-        "
	echo $SPACES "\`:oooossssssss                   .:ossssso.      "
	echo $SPACES "     -ssssssss                      -osssss-     "
	echo $SPACES "    /sssssssss                       \`/sssss:    "
	echo $SPACES "   :sssss/sss+                         /sssss.   "
	echo $SPACES "  \`syyyy/\`s/\`                           oyyyyo   "
	echo $SPACES "  /yyyys  \`                             -yyyyy\`  "
	echo $SPACES "  oyyyy/                                \`yyyyy.  "
	echo $SPACES "  syyyy:                                \`yyyyy.  "
	echo $SPACES "  +yyyy+                                :yyyys   "
	echo $SPACES "  -yyyyy.                              \`syyyy/   "
	echo $SPACES "   oyyyys\`                            \`syyyys    "
	echo $SPACES "   \`syyyys.                          -syyyys\`    "
	echo $SPACES "    \`syyyyy/\`                      .oyyyyyo\`     "
	echo $SPACES "      +yyyyyy+-                 \`:syyyyyy:       "
	echo $SPACES "       .+yyhhhhyo/-.\`     \`.-:+syhhhhyy/\`        "
	echo $SPACES "         \`/shhhhhhhhhhyyyyhhhhhhhhhyo-           "
	echo $SPACES "            \`:+yhhhhhhhhhhhhhhhyo/-              "
	echo $SPACES "                 .-:/+++++//:.\`                  "
	echo $SPACES
	echo $SPACES "      N O V A R E   T E C H N O L O G I E S       "
	echo $SPACES "              ${ORNG}Think. Build. Run.                 "
	echo -n "${NC}"
fi
