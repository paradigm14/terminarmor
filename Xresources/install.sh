#!/bin/bash

echo "Retrieving Xresources color themes"
if [ -d "base16-xresources" ]; then
	echo "Updating chriskempson/base16-xresources"
	cd "base16-xresources" || exit
	git pull origin master
	cd ..
else
	echo "Cloning chriskempson/base16-xresources"
	git clone 'https://github.com/chriskempson/base16-xresources'
fi

# if [ -d "temp" ]; then
# 	rm -rf ./temp
# fi

# mkdir temp
# cp ./base16-xresources/* ./temp

LINES=$(tput lines)
COLUMNS=$(tput cols)
WIDTH=$(( LINES - 15))
HEIGHT=$(( COLUMNS - 10))

until [[ -e $FILE ]]; do
	FILE=$(dialog --stdout --title "Choose a Xresource theme" --fselect "./base16-xresources/" $WIDTH $HEIGHT )
done

clear

echo "Copying Xresources to $HOME"
cp "./.Xresources" "$HOME/.Xresources"

echo "Using $FILE as theme"
cat "$FILE" >> "$HOME/.Xresources"

echo "Installing Xresources..."
xrdb "$HOME/.Xresources"

echo "Successfully installed Xresources"
