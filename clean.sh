#!/bin/bash

echo "Removing powerline-fonts/fonts"
rm -rf ./powerline-fonts/fonts

echo "Removing Xresources/base16-xresources"
rm -rf ./Xresources/base16-xresources

echo "Temporary files successfully deleted"
